import React from "react";
import { View , Text ,StyleSheet, TouchableOpacity} from "react-native";

const Task = (props)=>{

    return (
        <View style={styles.item}>
            <View style={styles.itemL}>
                <View style={styles.square}></View>
                <Text style={styles.ItemText}>{props.text}</Text>
            </View>
            <View style={styles.circule}></View>
       
        </View>
    )
} 
   const styles = StyleSheet.create({
     item:{
     backgroundColor:"#f5fcff",
     padding:15,
     borderRadius:10,
     flexDirection:"row",
     alignItems:"center",
     justifyContent:"space-between",
     marginBottom:20,

     },
     itemL:{     
        flexDirection:"row",
        alignItems:"center",
        flexWrap:"wrap"

     },
     square:{
    width:24,
    height:24,
    backgroundColor:"#ba6b6c",
    opacity:0.4,   
    borderRadius:5,
    marginRight:15,
},
     ItemText:{
        maxWidth:"80%",
     },
     circule:{
        width:12,
        height:12,
        borderColor:"#ba6b6c",
        borderWidth:2,
        borderRadius:5,

     }
    });
export default Task;