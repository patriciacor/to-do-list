import React,{useState} from 'react';
import { Platform, StyleSheet, Text, View, TextInput,KeyboardAvoidingView, TouchableOpacity, Keyboard } from 'react-native';
import Task from "./components/Task";
export default function App() {
  const[task,setTask]= useState();
  const[taskItem,setTaskItem]=useState([]);
  
  const addTask = ()=>{
    Keyboard.dismiss();
    setTaskItem([...taskItem,task])
    setTask(null);
  } 

  const  completeTask = (index)=>{
    let items1= [...taskItem];
    items1.splice(index,1);
    setTaskItem(items1);

  }
   return (
    <View style={styles.container}>
     <View style={styles.taskWrapper}>
      <Text style={styles.taskTitle}>Today tasks</Text>
     <View style={styles.items}>
      {
        taskItem.map((item,index)=>
        {
       
        return(
          <TouchableOpacity key={index} onPress={()=>completeTask(index)}>
               <Task  text={item}/>
          </TouchableOpacity>
        ) 

        })
      }
        
     </View>
     </View>

<KeyboardAvoidingView behavoir={Platform.OS=== "android" ? "padding":"height"} style={styles.writeNewTask}>
<TextInput style={styles.input} placeholder={"Write new task"} value={task} onChangeText={text => setTask(text)}></TextInput>
<TouchableOpacity onPress={()=> addTask()}>
  <View style={styles.addTask}>
    <Text style={styles.symbol}>+</Text>

  </View>
</TouchableOpacity>

</KeyboardAvoidingView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffcccb',

  },
  taskWrapper:{
    paddingTop:80,
    paddingHorizontal:20,
  },
  taskTitle:{
   fontSize:40,
   fontWeight:'bold',
   color:"#fcfffc",
  },
  items:{
   marginTop:30,
  },
  writeNewTask:{
position:"absolute",
bottom:60,
width:"100%",
flexDirection:"row",
justifyContent:"space-evenly",
alignItems:"center"
  },
  input:{
    paddingVertical:15,
    paddingHorizontal:15,
    backgroundColor:"#f5fcff",
    borderRadius:60,
    borderColor:"#ffcccb",
    borderWidth:1,
    width:250,

  },
  addTask:{
    width:60,
    height:60,
    backgroundColor:"#f5fcff",
    borderRadius:60,
    justifyContent:"center",
    alignItems:"center",
    borderColor:"#ffcccb",
    borderWidth:1,

  },
  symbol:{
   
  }
  
});
